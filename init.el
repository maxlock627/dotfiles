;; 
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
(package-refresh-contents))

(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; fix some ui
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(column-number-mode 1)

;; highlight parens
(show-paren-mode 1)

;; close pairs
(electric-pair-mode 1)

;; c style
(setq-default c-default-style "k&r")

;; c++ style comments for c
(add-hook 'c-mode-hook 'c-toggle-comment-style -1)

;; confirmation promt
(fset 'yes-or-no-p 'y-or-n-p)

;; disable auto save
(setq make-backup-files nil)
(setq auto-save-default nil)

;; set font
(set-frame-font "Hack-12")

;; vim keybindings
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-set-undo-system 'undo-redo)
  (evil-mode))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package evil-commentary
  :ensure t
  :config
  (evil-commentary-mode 1))

;; auto complete
(use-package company
  :ensure t
  :config
  (define-key company-active-map (kbd "<return>") nil)
  (define-key company-active-map (kbd "RET") nil)
  (define-key company-active-map (kbd "<tab>") #'company-complete-selection)
  (setq company-minimum-prefix-length 1)
  (setq company-idle-delay 0.1)
  (global-company-mode 1))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode))

;; mini buffer completion
(use-package vertico
  :ensure t
  :bind (:map vertico-map
              ("C-j" . vertico-next)
              ("C-k" . vertico-previous))
  :init (vertico-mode 1))

(use-package vertico-directory
  :after vertico
  :ensure nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package marginalia
  :ensure t
  :init (marginalia-mode 1))

(use-package anzu
  :ensure t
  :config
  (global-anzu-mode))

(use-package kuronami-theme
  :ensure t
  :config (load-theme 'kuronami 't))

;; (use-package gruber-darker-theme
;;   :ensure t
;;   :config (load-theme 'gruber-darker 't))

(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("61c169b0255f73c9a228c9593f208102d53b1c46246cbe76eff6eaf5bb94303b" "77f1e155387d355fbbb3b382a28da41cc709b2a1cc71e7ede03ee5c1859468d2" default))
 '(package-selected-packages
   '(kuronami autothemer sakura-theme kuronami-theme yasnippet company anzu marginalia orderless vertico evil-commentary evil-collection evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
